import React, { Component } from 'react';
import { Button, FormGroup, FormControl, InputGroup, Glyphicon } from 'react-bootstrap';
import './SideNav.css';

class SideNav extends React.Component {
 
  constructor(props) {
    super(props)
  }

  getSideBar() {
    return (<div style={{width:'30%'}}> 
        <div id="sidebar-wrapper">

          <ul className="sidebar-nav">
           
            <li onClick={this.props.helpNavTriggered} style={{background:!this.props.searchTriggered && !this.props.showProfileView ? '#2c3e50' : 'white'}} className="sidebar-brand">
              <a onClick={this.props.helpNavTriggered}  style={{height:'100%',textAlign:'center', color:!this.props.searchTriggered && !this.props.showProfileView ? 'white'  : '#999999' }} href="#">
                 <Glyphicon glyph="question-sign" />
                  &nbsp;Help
              </a>             
            </li>

            <li onClick={this.props.coursesNavTriggered} style={{background:this.props.searchTriggered && !this.props.showProfileView ? '#2c3e50' : 'white'}} className="sidebar-brand">
              <a  onClick={this.props.coursesNavTriggered}  style={{height:'100%',textAlign:'center', color:this.props.searchTriggered  && !this.props.showProfileView? 'white'  : '#999999' }} href="#">
                 <Glyphicon glyph="book" />
                 &nbsp;Courses                
              </a>
            </li>

            <li onClick={this.props.usersNavTriggered} style={{background:this.props.searchTriggered && !this.props.showProfileView ? '#2c3e50' : 'white'}} className="sidebar-brand">
              <a  onClick={this.props.usersNavTriggered}  style={{height:'100%',textAlign:'center', color:this.props.searchTriggered  && !this.props.showProfileView? 'white'  : '#999999' }} href="#">
                 <Glyphicon glyph="user" />
                 &nbsp;Users                
              </a>
            </li>

            <li onClick={this.props.settingsNavTriggered} style={{background:this.props.searchTriggered && !this.props.showProfileView ? '#2c3e50' : 'white'}} className="sidebar-brand">
              <a  onClick={this.props.settingsNavTriggered}  style={{height:'100%',textAlign:'center', color:this.props.searchTriggered  && !this.props.showProfileView? 'white'  : '#999999' }} href="#">
                 <Glyphicon glyph="cog" />
                 &nbsp;Settings                
              </a>
            </li>

            <li onClick={this.props.reportsNavTriggered} style={{background:this.props.searchTriggered && !this.props.showProfileView ? '#2c3e50' : 'white'}} className="sidebar-brand">
              <a  onClick={this.props.reportsNavTriggered}  style={{height:'100%',textAlign:'center', color:this.props.searchTriggered  && !this.props.showProfileView? 'white'  : '#999999' }} href="#">
                 <Glyphicon glyph="stats" />
                 &nbsp;Reports                
              </a>
            </li>

          </ul>
        </div>
      </div>)
  }

  render() {
    return (

      <div style={{overflow:''}} id="wrapper">
        {this.getSideBar()}
      </div>
    );
  }
}

export default SideNav;
