import React from 'react';
import { Button, SplitButton, DropdownButton, MenuItem, FormGroup, FormControl, InputGroup, Glyphicon,  } from 'react-bootstrap';
import "./TopNav.css";

class TopNav extends React.Component {
  
  constructor(props) {
    super(props);
    this.state = {
      showDropdown: false
    }
  }
  
  _handleClickProfileDropdownToggle() {
    this.setState({
      showDropdown: !this.state.showDropdown
    })
  }
  
  handleOnClickProfile() {
    this.setState({
      showProfileModal: true,
      inputUserSearch: null
    })
  }
  
  _handleInputChange() {
    var searchInput = this.refs.searchInput.getDOMNode().value.trim()
    this.props.searchInputChanged(searchInput)
    this.setState({
      searchInput: searchInput
    })
  }
  
  _handleClickProfileDropdownLeave() {
    this.setState({
      showDropdown: false
    })
  }
  
  _handleClickProfileDropdownEnter() {
    this.setState({
      showDropdown: true
    })
  }


  render() {
    return (
      <div id="topnav-wrapper" style={{overflow:'visible'}}>
         <nav className="navbar navbar-default">
         
          <div className="container-fluid">

          

             <ul  className="nav navbar-nav navbar-right">
                <li  onMouseLeave={this._handleClickProfileDropdownLeave.bind(this)} onMouseEnter={this._handleClickProfileDropdownEnter.bind(this)} onClick={this._handleClickProfileDropdownToggle.bind(this)}>
                  <a style={{ display:'flex',alignItems:'left',alignContent:'left',fontSize:16}} href="#">
                   Account Settings
                  </a>
                </li> 
              </ul>
            
             {/* <ul  className="nav navbar-nav navbar-right">
                <li  onMouseLeave={this._handleClickProfileDropdownLeave.bind(this)} onMouseEnter={this._handleClickProfileDropdownEnter.bind(this)} onClick={this._handleClickProfileDropdownToggle.bind(this)}>
                  <a style={{ display:'flex',alignItems:'center',alignContent:'center',fontSize:16}} href="#">
                    Welcome, Robert
                  </a>
                </li> 
              </ul>*/}

      
                <DropdownButton title="Organization" style={{fontSize:16}} noCaret="true">

                  <MenuItem eventKey="1" style={{fontSize:16}}>Action</MenuItem>
                  <MenuItem eventKey="2" style={{fontSize:16}}>Another action</MenuItem>
                  <MenuItem eventKey="3" style={{fontSize:16}}>Something else here</MenuItem>
                </DropdownButton>

     

               <div class="form-group">
                  <input type="text" class="form-control" placeholder="Search"/>
               </div>

              {/*<form className="navbar-form navbar-left" style={{display:'flex',alignCenter:'center'}} role="search">
                <div style={{display:'flex',flexDirection:'row'}} className="form-group">
                  <h5 style={{margin:0,display:'flex',alignItems:'center', paddingRight:10}}>
                      Search
                    <i className="fa fa-2x fa-arrow-right"></i>
                  </h5>
                   <input ref="searchInput" onChange={this._handleInputChange.bind(this)} style={{width:'500',display: 'flex', flex: 1, alignItems: 'center', alignContent: 'center', margin: 'auto'}} type="text" className="form-control" placeholder="" />
                </div>
              </form>*/}

       
        </div>
      </nav>
    </div>
    );
  }

}

export default TopNav