import React from 'react';
import './App.css';
import TopNav from './TopNav';
import SideNav from './SideNav';

class App extends React.Component {
 
    constructor(props) {
      super(props)

      this.state = {
        hasUserImage:false,
        userImageUrl:null,
        showProfileModal: false,
        searchTriggered: false
      }

    }

    onClickProfile() {
     
      this.setState({
        inputUserSearch: null,
        showProfileView: true
      })
    }

    onSearchInputChange(userInput) {
      this.setState({
          searchTriggered: true,
          showProfileModal: false,
          showProfileView:false,
          userInput
      })

    }

    handleMyOrderedNavTriggered() {
      this.setState({
        searchTriggered: true,
        showProfileView: false,
        showProfileModal:false
      })
    }

    handleGetAQuoteNavTriggered() {
      this.setState({
        searchTriggered: false,
        showProfileView: false,
        showProfileModal:false
      })
    }

	handleCloseModal(){
	this.setState({
	  showProfileModal:false
	})
	}

	handleOpenModal(){
	  this.setState({
	    showProfileModal:true
	  })
	}

  render() {
      return (
      	<div>
      		<TopNav/>
         
      		<SideNav showProfileView={this.state.showProfileView} getAQuoteNavTriggered={this.handleGetAQuoteNavTriggered.bind(this)} myOrderedNavTriggered={this.handleMyOrderedNavTriggered.bind(this)} searchTriggered={this.state.searchTriggered}/>
       		 {/*
       		<div style={{marginLeft:260}}>
      			<MainContent userInput={this.state.userInput} showProfileView={this.state.showProfileView} handleCloseModal={this.handleCloseModal.bind(this)} handleOpenModal={this.handleOpenModal.bind(this)} userName={this.props.userName} searchTriggered={this.state.searchTriggered} showProfileModal={this.state.showProfileModal}/>
        	</div>
            */}
    	</div>
    	)
  }
}

export default App